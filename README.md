# weRide
weRide- Making ride sharing easier.. This app uses ActionBarSherLock Library UI Components. 

You will need ActionBarSherLock library and Google Play Services to successfully compile the app. 

You can get the ActionBarSherLock Library from the link given below
https://bitbucket.org/zohaibu95/abs

You can download the Google play services from the Android SDK Manager.

After downloading import it into Eclipse and add these libraries as a reference in weRide project.

WIKI: [Documentation](https://www.dropbox.com/s/k244q3k3pfhyt20/weRide%20documentation.pdf?dl=0)

Installable APK: [Download](https://www.dropbox.com/s/bxaph0e1854ojil/weRide.apk?dl=0)

If you have any issue you can contact me on 'zohaib.ahmed.khan@outlook.com'.